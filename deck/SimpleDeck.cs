﻿using System;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("tests")]
namespace Deck
{
    public class SimpleDeck : IDeck
    {
        const int deckSize = 13 * 4;
        private readonly int _decks;
        private readonly IRandomHelper _random;
        internal protected readonly BitArray _deck;

        public SimpleDeck(IRandomHelper random) : this(random, 1)
        {
        }
        public SimpleDeck(IRandomHelper random, int decks)
        {
            _random = random;
            if (decks <= 0)
                throw new ArgumentOutOfRangeException(nameof(decks));

            _decks = decks;
            _deck = new BitArray(deckSize * decks);
            _deck.SetAll(true);

        }
        public Card Draw()
        {
            var next = _random.Next();
            while (!_deck[next] && _deck.Cast<bool>().Any(p => p))
                next = _random.Next();
            if (!_deck[next])
                throw new DeckException("end of game");

            _deck[next] = false;
            return new Card(next);
        }
    }
}
