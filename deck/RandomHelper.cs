using System;

namespace Deck
{
    public class RandomHelper : IRandomHelper
    {
        const int StandardDeckSize = 52;
        private Random _random = new Random();
        public int Next() => _random.Next(StandardDeckSize - 1);
    }
}
