﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("tests")]
namespace Deck
{
    public class BfsDeck : IDeck
    {
        const int deckSize = 13 * 4;
        private readonly int _decks;
        private readonly IRandomHelper _random;
        internal protected readonly BitArray _deck;

        public BfsDeck(IRandomHelper random) : this(random, 1)
        {
        }
        public BfsDeck(IRandomHelper random, int decks)
        {
            _random = random;
            if (decks <= 0)
                throw new ArgumentOutOfRangeException(nameof(decks));

            _decks = decks;
            _deck = new BitArray(deckSize * decks);
            _deck.SetAll(true);

        }
        public Card Draw()
        {
            var start = _random.Next();
            var queue = new Queue<int>();
            queue.Enqueue(start);
            while (queue.Count > 0) {
                var node = queue.Dequeue();
                if (_deck[node]) {
                    _deck[node]= false;
                    return new Card(node);
                }
                if (_deck.Cast<bool>().Any(p => p)) 
                    queue.Enqueue(_random.Next());
            }
            throw new DeckException("No More Cards");
        }
    }
}
