namespace Deck
{
    public interface IDeck{
        Card Draw();
    }
}
