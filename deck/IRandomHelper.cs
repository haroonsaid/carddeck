namespace Deck
{
    public interface IRandomHelper
    {
        int Next();
    }
}
