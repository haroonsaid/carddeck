using System;

namespace Deck
{
    public class Card
    {
        const int SuitSize = 13;
        public int Value { get; private set;}
        public Card(int value)
        {
            if (value < 0 || value > (SuitSize * 4) - 1)
                throw new ArgumentOutOfRangeException(nameof(value));
            var quotient = value/SuitSize;
            this.Suit = (Suits)Enum.Parse(typeof(Suits), quotient.ToString());
            this.Value = (value % SuitSize) ;
        }

        public Card()
        {
        }

        public Suits Suit { get; internal set; }
    }
}
