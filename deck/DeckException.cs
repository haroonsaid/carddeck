using System;
using System.Runtime.CompilerServices;

namespace Deck
{
    public class DeckException : Exception
    {
        public DeckException(string message) : base(message) { }

    }
}
