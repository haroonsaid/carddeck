﻿using System;
using Xunit;
using Deck;
using System.Diagnostics;
using Xunit.Abstractions;

namespace tests
{

    public class UnitBfsTests
    {
        private readonly ITestOutputHelper output;

        public UnitBfsTests(ITestOutputHelper output)
        {
            this.output = output;
        }
        [Fact]
        public void InvalidRandom()
        {
            var helper = new Moq.Mock<IRandomHelper>();
            helper.Setup(p => p.Next()).Returns(-1);
            var sut = new BfsDeck(helper.Object);
            Assert.Throws<ArgumentOutOfRangeException>(() => sut.Draw());
        }
        [Fact]
        public void CardOutOfRangeLessThanZero()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Card(-1));
        }
        [Fact]
        public void CardOutOfRangeGraterThanMax()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Card(99));
        }
        [Fact]
        public void FirstCard()
        {
            var expected = new Card(0);
            var helper = new Moq.Mock<IRandomHelper>();
            helper.Setup(p => p.Next()).Returns(0);
            var sut = new BfsDeck(helper.Object);
            var actual = sut.Draw();
            Assert.Equal(expected.Suit, actual.Suit);
            Assert.Equal(expected.Value, actual.Value);
        }
        [Fact]
        public void LastCard()
        {
            var expected = new Card(51);
            var helper = new Moq.Mock<IRandomHelper>();
            helper.Setup(p => p.Next()).Returns(51);
            var sut = new BfsDeck(helper.Object);
            var actual = sut.Draw();
            Assert.Equal(expected.Suit, actual.Suit);
            Assert.Equal(expected.Value, actual.Value);
        }
        [Fact]
        public void PickCard()
        {
            var expected = new Card(25);
            var helper = new Moq.Mock<IRandomHelper>();
            helper.Setup(p => p.Next()).Returns(25);
            var sut = new BfsDeck(helper.Object);
            var actual = sut.Draw();
            Assert.Equal(expected.Suit, actual.Suit);
            Assert.Equal(expected.Value, actual.Value);
        }
        [Fact]
        public void PickAnyCard()
        {
            var helper = new RandomHelper();
            var expected = new Card(helper.Next());
            var sut = new BfsDeck(helper, 1000000);
            sut._deck.SetAll(false);
            var index = ((int)expected.Suit * 13) + expected.Value;
            sut._deck[index] = true;
            var sw = new Stopwatch();
            sw.Start();
            var actual = sut.Draw();
            using (var writer = new System.IO.StreamWriter(System.Console.OpenStandardOutput()))
                writer.WriteLine($"Bfs Elapsed:{sw.ElapsedTicks}");
            Assert.Equal(expected.Suit, actual.Suit);
            Assert.Equal(expected.Value, actual.Value);
        }
        [Fact]
        public void PickedAllException()
        {
            var helper = new RandomHelper();
            var sut = new BfsDeck(helper);
            sut._deck.SetAll(false);
            Assert.Throws<DeckException>(() => sut.Draw());
        }
    }
}
